import { format} from 'date-fns'

function useFormattedDate(str, outputFormat='yyyy-MM-dd HH:mm:ss') 
    {
    
    const formatted = str ?  format(new Date(str), outputFormat) : ''
    

    return formatted
   }


export default useFormattedDate   