import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import './App.css';
import Nav from './components/Nav';
import AddPost from './pages/AddPost';
import SinglePost from './pages/SinglePost';
import AppPosts from './pages/AppPosts';

function App() {
  return (
    <div className="App">
  <Router>
        <Nav/>

        <div className="container">
        <Switch>
          
            <Route path='/' exact>
                <Redirect to="/posts" />
            </Route>

            <Route path='/posts' exact>
                <AppPosts/>
            </Route>


            <Route path='/post/:id' exact>
                <SinglePost/>
            </Route>

            <Route path='/edit/:id' exact>
                <AddPost/>
            </Route>

            <Route path='/add' exact>
                <AddPost/>
            </Route>
          
        </Switch>
        </div>
      </Router>

    </div>
  );
}

export default App;
