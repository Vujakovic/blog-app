import React, { useState } from 'react'


function AddComment({addComment}) {

    const [comment, setComment] = useState({
        text:''
    })

    const handleSubmit = (e)=>{
        e.preventDefault()

        addComment(comment)
    }


  return (
    <div>
        <form className='d-block col-md-6' onSubmit={handleSubmit}>
                <div className="form-group text-left">
                    <label htmlFor="title">Leave comment:</label>
                    <input required minLength="2" type="text" 
                    className="form-control" 
                    placeholder="Your comment" 
                    id="title"
                    value={comment.text}
                    onChange={e => setComment({...comment, text: e.target.value})}/>
                </div>

                <button type="submit" className="btn btn-primary">Add Comment</button>
                
            </form>
    </div>
  )
}

export default AddComment