import React from 'react'
import {Link} from 'react-router-dom'

function Nav() {
  return (
    <div>
        <nav className="navbar navbar-expand-sm bg-dark">


            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to='/posts'>Post</Link>
                </li>

                <li className="nav-item">
                    <Link className="nav-link" to='/add'>Add</Link>
                </li>
                
           </ul>

        </nav>
    </div>
  )
}

export default Nav
