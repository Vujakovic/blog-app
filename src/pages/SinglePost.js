import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import AddComment from '../components/AddComment'
import useFormattedDate from '../hooks/UseFormattedDate'
import PostService from '../service/PostService'


function SinglePost() {
    const {id} = useParams()
    const [post, setPost] = useState({})

    useEffect(() => {
      
        const fetchPost = async () => {
          const data = await PostService.get(id);
          
          setPost(data);
        
        };
        fetchPost();
      }, [id]);


      const addNewComment = async(newComment)=>{
          const data = await PostService.addComment(newComment, id)

          setPost({...post, comments:[...post.comments, data]})
      }

      
      
      const cr= useFormattedDate(post.createdAt)

  return (
    <div>
        <h1>{post.title}</h1>
        <small>{cr}</small>
        <p>{post.text}</p>

        <AddComment  addComment={addNewComment}/>

        <div className="comments mt-2">
        {post.comments && post.comments.map((comment,index)=>(
              <div key={comment.id} className="card p-2 mb-2">
                  <span><strong>#{index+1}</strong></span>
                  <br />
                  <p>{comment.text}</p>
               </div> 
            ))}
        </div>
    </div>
  )
}

export default SinglePost