import React, { useEffect } from 'react';
import { useState } from 'react';
import {Link} from 'react-router-dom';
import PostService from '../service/PostService'


function AppPosts() {
    const [posts, setPosts] = useState([])

    useEffect(() => {
        
        const fetchPosts = async () => {
          const data = await PostService.getAll();

          setPosts(data);
        };

        fetchPosts();
      }, []);

      
  
    const deletePost = async(id)=>{
      const data = await PostService.delete(id);

      if (data.count > 0) {
        setPosts(posts.filter((post) => post.id !== id));
      }
    }


  return (
    <div className='postsList'>
            {posts.map(post=>(
        <div key={post.id} className="card post p-2 mb-2">
                <div className="content">
                <span><Link to={`/post/${post.id}`}><h3>{post.title}</h3> </Link></span>
                <p>{post.text}</p>
                </div>

                <div className="button-edit">
                    <Link to={`/edit/${post.id}`} type='button' className='btn btn-info'>Edit</Link>
                    <button className="btn btn-danger ml-2" onClick={()=>deletePost(post.id)}>Delete</button>
                </div>
        </div>
            ))}
    </div>
  )
}

export default AppPosts