import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useHistory } from 'react-router-dom'
import PostService from '../service/PostService'

function AddPost() {
    const history = useHistory()
    const {id} = useParams()

    const [newPost, setNewPost] = useState({
        title: '',
        text: ''
      });


    const handleSubmit = async (e) => {
        e.preventDefault();

        if(id){
          
          await PostService.edit(id, newPost)
        }else{
          await PostService.add(newPost);
        }

          history.push('/posts')
        }


    const resetForm = ()=>{
        setNewPost({
            title:'',
            text:''
        })
    }

    useEffect(() => {
      const fetchPost = async()=>{
        const{id: _, ...restPost} = await PostService.get(id)

        setNewPost({...restPost})
      }
    
      if(id){
        fetchPost()
      }
      return () => {
        
      }
    }, [id])
    


  return (
    <div>
        <form className='d-block col-md-6' onSubmit={handleSubmit} onReset={resetForm}>
                <div className="form-group">
                    <label htmlFor="title">Title:</label>
                    <input required minLength="2" type="text" 
                    className="form-control" 
                    placeholder="Post Title" 
                    id="title"
                    value={newPost.title}
                    onChange={e => setNewPost({...newPost, title: e.target.value})}/>
                </div>

                <div className="form-group">
                    <label htmlFor="text">Text:</label>
                    <input required minLength="2" maxLength="300" type="text" 
                    className="form-control" 
                    placeholder="Post Text" 
                    id="text"
                    value={newPost.text}
                    onChange={e => setNewPost({...newPost, text: e.target.value})}/>
                </div>

                <button type="submit" className="btn btn-primary">{id ? 'Edit' : 'Add'}</button>
                <button className="btn btn-danger ml-2" type='reset'>Reset</button>
            </form>

    </div>
  )
}

export default AddPost